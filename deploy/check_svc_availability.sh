#!/bin/sh

TARGET=$(oc get routes dummyhttp2 -n timon 2>/dev/null | awk 'FNR>1{print $2}')

if test -z "$TARGET"; then
    echo failed resolving test deployment address
    exit 1
fi

while :
do
    if ! curl --connect-timeout 2 http://$TARGET/ >/dev/null 2>&1; then
	echo FAILED on `date`
    fi
    sleep 0.1
done
